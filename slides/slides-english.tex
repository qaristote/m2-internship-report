\documentclass{beamer} \usecolortheme{seahorse}
\setbeamertemplate{footline}[frame number] \setbeamertemplate{navigation
  symbols}{} \setbeamertemplate{section page} {
  \begin{centering}
    \begin{beamercolorbox}[sep=12pt,center]{part title}
      \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}
  \end{centering}
}


%%%%%%%%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%%%%%%%%%
% maths
%% symbols
\usepackage{amsmath, amssymb, amsthm} \usepackage{stmaryrd} \usepackage{wasysym}
%% diagrams
\usepackage{tikz-cd} \usetikzlibrary{decorations.markings}
\tikzset{negated/.style={ decoration={markings, mark= at position 0.5 with {
        \node[transform shape,xscale=.8,yscale=.4] (tempnode) {$\slash$}; } },
    postaction={decorate} } } \usepackage{wrapfig}
% algorithms
\usepackage{algorithm, algorithmic}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
% links
\usepackage[nameinlink]{cleveref}
% bibliography
\usepackage[backend=biber, style=ieee, citestyle=numeric-comp]{biblatex}
\addbibresource{../biblio.bib} \renewcommand*{\bibfont}{\footnotesize}
% misc
\usepackage[english]{babel} \usepackage{csquotes} \usepackage{subcaption}
\captionsetup[subfigure]{subrefformat=simple} \usepackage{appendixnumberbeamer}

%%%%%%%%%%%%%%%%%%%% COMMANDS %%%%%%%%%%%%%%%%%%%%
% operators and commands
%% monoids
\DeclareMathOperator{\lgcd}{lgcd} \DeclareMathOperator{\red}{red}
\DeclareMathOperator{\Irr}{Irr} \DeclareMathOperator{\rk}{rk}
\newcommand{\dual}[1]{{#1}^{op}} \newcommand{\inv}[1]{{#1}^{-1}}
\newcommand{\invertibles}[1]{{#1}^{\times}}
\newcommand{\LeftDivide}{\textsc{LeftDivide}}
\newcommand{\UpToInv}[1]{\textsc{UpToInv#1}} \newcommand{\LGCD}{\textsc{LGCD}}
\newcommand{\Red}{\textsc{Red}}
%% wqos
\DeclareMathOperator{\down}{{\downarrow}} \DeclareMathOperator{\up}{{\uparrow}}
\newcommand{\Ord}{\mathbf{Ord}} \newcommand{\sobr}[1]{\widehat{#1}}
%% functions
\DeclareMathOperator{\im}{im} \DeclareMathOperator{\id}{id}
%% automata
\DeclareMathOperator{\Reach}{Reach} \DeclareMathOperator{\Obs}{Obs}
\DeclareMathOperator{\Min}{Min} \DeclareMathOperator{\Total}{Total}
\DeclareMathOperator{\Prefix}{Prefix} \newcommand{\Auto}[1]{\mathbf{Auto}_{#1}}
\newcommand{\BiAuto}[1]{\mathbf{BiAuto}_{#1}} \newcommand{\In}{\mathtt{in}}
\newcommand{\Out}{\mathtt{out}} \newcommand{\St}{\mathtt{st}}
\newcommand{\A}{\mathcal{A}} \newcommand{\B}{\mathcal{B}}
\renewcommand{\H}{\mathcal{H}} \newcommand{\J}{\mathcal{J}}
\renewcommand{\L}{\mathcal{L}}
%% rings
\DeclareMathOperator{\sat}{sat} \newcommand{\Mod}[1]{#1\mathbf{Mod}}
\newcommand{\K}{\mathbb{K}} \newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}} \newcommand{\Q}{\mathbb{Q}}
%% categories
\DeclareMathOperator{\Obj}{Obj} \DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\codim}{codim} \DeclareMathOperator{\reldim}{reldim}
\DeclareMathOperator{\Id}{Id} \newcommand{\Set}{\mathbf{Set}}
\newcommand{\Kl}{\mathbf{Kl}} \renewcommand{\Vec}[1]{#1\mathbf{Vec}}
\newcommand{\C}{\mathcal{C}} \newcommand{\E}{\mathcal{E}}
\newcommand{\I}{\mathcal{I}} \newcommand{\M}{\mathcal{M}}
\renewcommand{\O}{\mathcal{O}} \newcommand{\T}{\mathcal{T}}
% misc
\renewcommand{\P}{\mathcal{P}} \newcommand{\N}{\mathbb{N}}
\newcommand{\card}[1]{\left| #1 \right|}
\newcommand{\Eval}[1]{\textsc{Eval}_{#1}}
\newcommand{\Equiv}[1]{\textsc{Equiv}_{#1}} \newcommand{\word}[1]{\triangleright
  #1 \triangleleft} \newcommand{\Surj}{\mathrm{Surj}}
\newcommand{\Inj}{\mathrm{Inj}} \newcommand{\Tot}{\mathrm{Tot}}
\newcommand{\Inv}{\mathrm{Inv}} \newcommand{\sem}[1]{\left\llbracket #1 \right\rrbracket}

\newcommand{\includefigure}[2][]{\includegraphics[#1]{../figures/#2/figure.pdf}}
\newcommand{\makesection}{\begin{frame}\sectionpage\end{frame}}
  
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Applications of a categorical framework for minimization and active
  learning of transition systems}

\author{Quentin Aristote, ENS Paris, PSL University \\ {\footnotesize supervised
    by} Daniela Petri\c{s}an, IRIF, Université Paris-Cité}

\date{September 4th, 2022}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Preliminary: categorical framework for automata minimization and learning}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Preliminary: automata as functors}
  \begin{center}
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZAJgBpiBdUkANwEMAbAVxiRAB12BBACk4GUcAShABfUuky58hFABZyVWoxZtiAPWBd1AKlFiJIDNjwEiABkXV6zVohDadBySZlEAjFeW22zo1NNZElI5JRtVe3cAAgBeKM5gKJ149n1xF2kzFDJzMJU7EGJYlMTOACMIHFIUnAg0FLSlGCgAc3giUAAzACcIAFskSxBaweoGCDqiYgB2S07GOBglBjoymAYABQC3e26sFoALHBBrfLZOXjoRdJAe-qRPYYgHsYm0IgBOOYWlsdX1rauLIgPaHY6nHz2AAyKT6dDQcFqKSwYBowDooihfjuA0QZCeSAAzGMUQUoHQ4Admtjerj8SM8a9JtlZqR5gxFst-pttsDQUcTt4IiAAO6w+GIiBREV0Gn3RAKAmMoUFZKcOEIpGcGAIrDjQjUNZgKBE8w3HFIRUMobhAoXPjsHB7OhgFoMGD84Ry3FDBkAVghwvtnCdWBdbvWME6XvNtKQ9OeiADKrYYvVEq17ChDpFEG6UGAItEIkNMGNptj8se-sDBRh6c1Uuz2t1+pLICNJsQAFpCUMVmseUDZCD9gLK7irYnHra2Gr2BrJSksaXyz2+39B4DMiPPd7RkrHgxSWxyZTqaIKKIgA
    \begin{tikzcd}
      &    & {2 = \{ \bot, \top \}}                                                                                                               &  &                                                                                                                              \\
      &    &                                                                                                                                      &  &                                                                                                                              \\
      A^* \arrow[r, dashed] \arrow["w \mapsto wa"', loop, distance=2em, in=215, out=145] \arrow[rruu, "w \mapsto \L(\word{w})", bend left] & {} & \A(\St) \arrow["\A(a)"', loop, distance=2em, in=215, out=145] \arrow[uu, "\A(\triangleleft)"] \arrow[rr, dashed]                     &  & 2^{A^*} \arrow["L \mapsto \inv{a}L"', loop, distance=2em, in=35, out=325] \arrow[lluu, "L \mapsto L(\epsilon)"', bend right] \\
      &    &                                                                                                                                      &  &                                                                                                                              \\
      & & 1 = \{ * \} \arrow[lluu, "* \mapsto \epsilon", bend left] \arrow[uu,
      "\A(\triangleright)"] \arrow[rruu, "* \mapsto \L"', bend right] & &
    \end{tikzcd}
  \end{center}
  \begin{itemize}
  \item diagram $(
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
    \begin{tikzcd}
      \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
      in=125, out=55] \arrow[r, "\triangleleft"] & \Out
    \end{tikzcd}
    )$ instanciated in $\Set$
  \item language $\L$ = restriction to $(
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAPJMcIXlJhQA5vCKgAZgCcIAWyRkQOCEgnTmrRB3YB3CPqjBHvFRV5A
    \begin{tikzcd}
      \In \arrow[r, "\word{w}"] & \Out
    \end{tikzcd}
    )$
  \item \emph{weighted automaton} = diagram instanciated in $\Vec{\K}$, etc.
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Preliminary: minimal automaton recognizing a language}
  \begin{itemize}
  \item $(\E, \M) = (\Surj, \Inj)$ is a \emph{factorization system} in $\Set$:
    $\A(\St) \xrightarrow[]{f} \A'(\St)$ factorizes uniquely as
    \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZARgBoAGAXVJADcBDAGwFcYkQAdDrAWwAoAZgEoQAX1LpMufIRQAmCtTpNW7LgEEA5Hy4BlHCPGTseAkXKKaDFm0ScO6nR32GlMKAHN4RUAIBOEDxIFiA4EEhkIAAWMPRQSGDMjIxWKrYgPAAEXFhg2RwAsiA0OPRYjOyl5WISIP6BSAqh4Ygh1qp2MPm5+QCixdGx8XY4AO4QMXEIopSiQA
      \begin{tikzcd}
        \A(\St) \arrow[r, "e \in \E", two heads] & \im(f) \arrow[r, "m \in \M",
        tail] & \A'(\St)
      \end{tikzcd}
    \end{center}
  \item for automata, $\Reach(\Obs \A) = \Obs(\Reach \A) = \Min \L$
    \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoBGAXVJADcBDAGwFcYkQBBAPQCoQBfUuky58hFOQrU6TVuwA68gEox6AYwAWijgKEgM2PASIAmUsWkMWbRCG27hhsUQDMUmlbm3FAeQBGcPaCjqLGKAAs7jLW7CZcwNw8-A76IkbiyGYmlrI2dvIAslhgigAyAtIwUADm8ESgAGYAThAAtkhkIDgQSJIgGqpQ7DgA7hAD9FAIwSDNbb003UhmXfRYjMNrGzNz7YgArIs9iG6r65vnOy17p0uIkWcbtjhbKbvLR0inE0PPYz-TPTvRCdO4rTx5EYAAkUrXoaDg3Rh8g4AApFDgmlh6GBqowYFjqhocFCRgBKEA0PwwMC-FzEK7zRArO4PCHsODIuEIpFlAD6CCpNLpDKB1wWXWOh36g2G-0GgMa4pBn3uQtpSAAtAA2Trs7zyOE4DRNVrAAoATw060YmoAcgToDB+IhobD4YiIMjirRgCN+GVKSBGPRqYwAAppZy2QnEir8IA
      \begin{tikzcd}
        &                                                 & \A \arrow[rd, two heads] \arrow[rrd, "s \mapsto \L_s", bend left] &                        &         \\
        A^* \arrow[r, two heads] \arrow[rru, "w \mapsto \A(\triangleright w)", bend left] \arrow[rrrr, "\mathrm{Myhill-Nerode}:w \mapsto \inv{w}\L"', bend right=60] & \Reach\A \arrow[ru, tail] \arrow[rd, two heads] &                                                                   & \Obs\A \arrow[r, tail] & 2^{A^*} \\
        & & \Min\L \arrow[ru, tail] & &
      \end{tikzcd}
    \end{center}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Preliminary: learning a rational language}
  \begin{itemize}
  \item Input: $\Eval{\L}$ computes $\L(\word{w})$; $\Equiv{\L}$ checks whether
    $\A$ recognizes $\L$ or finds a $w$ s.t. $\L(\word{w}) \neq \A(\word{w})$
  \item Output: $\Min \L$
  \item maintain prefixes $Q \subset A^*$, suffixes $T \subset A^*$
  \item knowledge of $\L$ restricted to $Q(A \cup \epsilon)T = (
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAEYQBfU9TXfIRTtyVWoxZsATNzEwoAc3hFQAMwBOEALZIyIHBCQjxzVohAAdCwBkAFFYDuEdVGABHOji4BKWVyA
    \begin{tikzcd}
      1 \arrow[r, "\L(\word{qat})"] & 2
    \end{tikzcd}
    )$
  \item represented by the \emph{biautomaton} $\B =
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAEYQBfU9TXfIRTtyVWoxZsAigH1gAegA6i7AFs5AFQAEygMZM0WgIIauXbrxAZseAkQBMo6vWatEIABRSdi-YalGAJRySipY6qYWfDaCRADMTuKubPbcYjBQAObwRKAAZgBOEKpIZCA4EEgiSZLuygBCHso4BVh0YJkMMK2ZABY4WgCOgVEghcVIjuWViAk1biANHgPNre2dMF15OCM8+UUliNUVk9RwvVjbVc4SC0vKMGjYDAQj1Ax0AEabAAr8tkIQD1+qNxodjjMpudLjgkABaaouWqLRSNOi7ChcIA
    \begin{tikzcd}
      1 \arrow[r, "\B(\triangleright q)"] & Q_{/\sim_{T \cup AT}} \arrow[r,
      "\B(\epsilon)"', shift right] \arrow[r, "\B(a)", shift left] & (Q \cup
      QA)_{/\sim_T} \arrow[r, "\B(t \triangleleft)"] & 2
    \end{tikzcd}
    $
  \item add words to $Q$ and $T$ until $\B(\epsilon) \in \Surj \cap \Inj$
  \item merge $\B$ into an automaton and feed it to $\Equiv{\L}$
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Preliminary: contributions to this framework}
  \begin{itemize}
  \item expressing the complexity with categorical notions
    \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoBGAXVJADcBDAGwFcYkQBFAfWAHoAdftgC2XQQBkAviEml0mXPkIpyFanSat2ACg4ACQQGNmaPRwCCASh4ChWUROmz52PASIAmUsXUMWbRBBzAD0AKhtBETF+KRk5EAxXJU81Gj8tQKMoCBwESXUYKABzeCJQADMAJwhhJDIQHAgkVQb6LEZ2HDaO5xAqmuaaRqQAZiHuzone-trEeuHEL1b2yZWaACMYMChR4mnq2ZaFpa6VwNOOja2dxBG9+JnRoabF8bPlnspJIA
      \begin{tikzcd}
        &                                                                    & A^*_{/\sim_\L}         \\
        Q_{/\sim_\L} \arrow[r, tail] \arrow[rru, tail, bend left] & (Q \cup
        QA)_{/\sim_\L} \arrow[r, tail] \arrow[ru, tail, bend left] & \cdots
        \arrow[u, tail]
      \end{tikzcd}
    \end{center}
  \item categorical algorithm for ``improper'' learning
  \item categorical algorithm for computing $\Reach$ and $\Obs$
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Monoidal transducers}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Transducers with output in a monoid $M$}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includefigure{transducer}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{itemize}
      \item
        $(% https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
        \begin{tikzcd}
          \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
          in=125, out=55] \arrow[r, "\triangleleft"] & \Out
        \end{tikzcd})$ in $\Kl(\T_M)$
      \item $\Kl(\T_M)$ = sets with functions $X \nrightarrow Y \equiv X
        \rightarrow M \times Y + 1$
      \item $\A
        = % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB13gACAKm84BfEINLpMufIRQBGclVqMWbTgEEAFJwDKOAJQixIDNjwEiAJnnV6zVog5c+A9sMEKYUAObwioAGYAThAAtkhkIDgQSHKKtirsGpw4AVh0YJ4MMCmeABZ6Bv5BoYgxkUiWscr2aprsyanpmZl++aKFIdHUZSXUDBAQaEQyABxkfoxwMAoMdABGMAwAChKm0iDZeSDWSnYOGnT6boJAA
        \begin{tikzcd}
          \{ * \} \arrow[r, negated, "\A(\triangleright)"] & \A(\St) \arrow[r,
          negated, "\A(\triangleleft)"] \arrow[negated, "\A(a)"', loop,
          distance=2em, in=125, out=55] & \{ * \}
        \end{tikzcd}$
      \item $\L
        = % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB13gACAKm84BfEINLpMufIRQBGclVqMWbTj35CRCmFADm8IqABmAJwgBbJGRA4ISOYuatEHdgEEAFJxzGsdMDoYYHx0ACxxuAHcBdm9ff0DAwxwASk1BIA
        \begin{tikzcd}
          \{ * \} \arrow[r, negated, "\A(\triangleright w \triangleleft)"] & \{
          * \}
        \end{tikzcd}$
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Condition for the final transducer to exist}
  \begin{itemize}
  \item $1 = \{ * \}$ has an $A^*$-copower in $\Kl(\T_M)$ when every $(L_w : 1
    \nrightarrow 1)_{w \in A^*}$ factorizes uniquely as
    \[ L_w
      = % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAEYQBfU9TXfIRTtyVWoxZsAOlICSAJ3kAKAIIA9AFSkABAFkAlN14gM2PASIAmUdXrNWiDtzEwoAc3hFQAM3kQAtkhkIDgQSCIgDFhgDiBQdHAAFq4gthKxMgx0YG4MMNoAMgD6AO7aMvLZuTBFwGUy0drqGlxGPn6BiBGhSNbi9tJSaFilzlxAA
      \begin{tikzcd}
        1 \arrow[r, negated, "\langle L_w \rangle_{w \in A^*}", dashed] &
        {\Irr(A^*, M)} \arrow[r, negated, "\pi_w"] & 1
      \end{tikzcd}
    \]
  \item $\Leftrightarrow$ there are $\lgcd : (A^* \rightarrow M + 1) \rightarrow
    M + 1$ and $\red : (A^* \rightarrow M + 1) \rightarrow (A^* \rightarrow M +
    1)$ s.t.
    \begin{itemize}
    \item $L = \lgcd(L)\red(L)$
    \item $\upsilon \red K = \nu \red L$ implies $\upsilon = \nu$ and $\red K =
      \red L$
    \end{itemize}
  \item true in particular when $M$ is cancellative and every family has a
    unique greatest common left divisor
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}[fragile]
  \frametitle{Initial and final transducers recognizing a language}
  \begin{center}
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoAmAXVJADcBDAGwFcYkQAdD4AAgCoeXAL4ghpdJlz5CKACwVqdJq3ZdeA4aPEgM2PASLlSxRQxZtEIAIIA9Plol7ph0rNPKLnDgEkATr4AKWz5SHgBZAEpRRRgoAHN4IlAAM18IAFskIxAcCCyaRggINCIARgAOMmSmOBhFRnoAIxhGAAVJfRkQXyw4gAscEBozFUsAd0EOdPo0OFyeAK4YWaxCsFCx+iixFLTMxABmGlykI5BC4qIqmrqCppb2pwNLHv7B4Y92ABlJ6dn5xYcRhxADGUEBWDAtGA9CEXwioS4vliEKhMLhEW22lSGSQZByeUQ2RGng0UxmcwgCyWKzWoRgWN2uKJx0JpQ+5nYEy4f0p1I4X0BYwgvigwDGQgR-EZIBx+3xJ0OHNGIDJvIBXGBYMmX0RHGRUB1UTuzTaHWcL16Awcsr2+QJp2Vnm55P+VMBWvBXEh0IlXHheoNqN9Qn9mKG53uZqeXUh2FgNrljodiHZSk5lh+PIpAMFDNCfGNkdNjykz26VsGQkoQiAA
    \begin{tikzcd}
      &  & A^* \arrow["{w \mapsto (\epsilon, wa)}"', loop, distance=2em, in=125, out=55] \arrow[rrdd, "{w \mapsto (\L(\word{w}), *)}"] \arrow[dddd, "{w \mapsto (\lgcd(\inv{w}\L), \red(\inv{w}\L))}" description] &  &         \\
      &  &                                                                                                                                                                                                         &  &         \\
      \{ * \} \arrow[rruu, "{* \mapsto (\epsilon, e)}"] \arrow[rrdd, "{* \mapsto (\lgcd \L, \red \L)}"'] &  &                                                                                                                                                                                                         &  & \{ * \} \\
      &  &                                                                                                                                                                                                         &  &         \\
      & & {\Irr(A^*, M)} \arrow["{L \mapsto (\lgcd(\inv{a}L),
        \red(\inv{a}L))}"', loop, distance=2em, in=305, out=235] \arrow[rruu,
      "{L \mapsto (L(e), *)}"'] & &
    \end{tikzcd}
  \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}[fragile]
  \frametitle{The factorization system on $\Kl(\T_M)$}
  \begin{center}
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BBEAX1PSZc+QigCM5KrUYs2nACoQcjAASce-QdjwEiAJknV6zVog7sACgCcYAMywAPNdz4CQGbSKIBmQ9JNy7ADyAEZwzhpuHsK6KAAsfsayZpwAklZWABRcAHoAVKQqALIAlK5aMaIkpGJSSaYg5e5COlW+tUYyDU3RrUQJHf7JjZrNnrHVenVdbD0tXigGU50BZnPjVQnLQ92jvQvV3tOrI1HzExJHK8Prlf2kVzuzvFIwUADm8ESgtlYQALZIMggHAQJASEAACxgdCgbBwAHcINDYQhRr8AeDqKCkAYoTC4WZEciCQhsXQsAx4RSqei-oDEHicYhfCCadTKSBqCjCSCkTy0W4MQzWcyEmzOUT2XTMYgAKzYsGIABs3IJbEgYFY1AYdBCMAYFnOohAWDA2FgXKeKXYAGUmFYAFZNYVIVUgpUAdjVsI1BG1IF1+sNxrYZotAfqgVSYEdzgAxnQ0M4YzQE0nnIocC76UgAByKpAATh9vM1AaDBqNGzD5qwluuDU49qd6eTaVjOdlJY94OBPL9WqtlZDNbM4frkZmNtTbczSi7DLEEOZYjxI+rd3HdYb+N9ZnLVqjNpbcc4ifb7BjZ-YF5TYBoi-BTKVYlZx-MWeHeqroe3EatAcD39PgKF4IA
    \begin{tikzcd}[row sep=tiny]
      \A \arrow[r, two heads]                                        & \Total \A \arrow[r, two heads, tail]        & \Prefix \A \arrow[r, two heads, tail]                & \Obs \A \arrow[r, tail]                                       & {\Irr(A^*, M)} \\
      {} \arrow[rrr, "\Surj" description, no head]                   &                                             &                                                      & {} \arrow[r, "\Inj \cap \Inv \cap \Tot" description, no head] & {}             \\
      {} \arrow[rr, "\Surj \cap \Inj" description, no head]          &                                             & {} \arrow[rr, "\Inv \cap \Tot" description, no head] &                                                               & {}             \\
      {} \arrow[r, "\Surj \cap \Inj \cap \Inv" description, no head] & {}
      \arrow[rrr, "\Tot" description, no head] & & & {}
    \end{tikzcd}
  \end{center}
  \begin{figure}
    \centering
    \begin{subfigure}{.55\linewidth}
      \includefigure[width=\linewidth]{transducer_prefix}
      \caption{$\Prefix \A$}
    \end{subfigure}
    \begin{subfigure}{.43\linewidth}
      \includefigure[width=\linewidth]{transducer_min}
      \caption{$\Obs \A$}
    \end{subfigure}
  \end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}
  \frametitle{Algorithms for computing minimal transducers}
  \begin{itemize}
  \item monoid $M$ has to be \emph{effective} and \emph{noetherian}
  \item learning
    \begin{itemize}
    \item similar to [Vilar, 96]
    \item polynomial number of calls to $\Eval{\L}$ and $\Equiv{\L}$
    \end{itemize}
  \item minimizing a transducer $\A$
    \begin{itemize}
    \item computing $\Reach$, $\Total$, $\Prefix$ and $\Obs$
      \begin{itemize}
      \item $\Reach$, $\Total$: depth-first searches
      \item $\Prefix$: fixpoint $\lgcd s = \lgcd{s} \wedge (s \ocircle a)
        \lgcd(s \cdot a)$
      \item $\Obs$: Moore's algorithm, up to invertibles
      \end{itemize}
    \item $O(n^3m)$
    \item implementation using \texttt{OCamlGraph.Fixpoint}
    \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Other applications of the framework}
\makesection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Weighted automata over rings}
  \begin{figure}
    \centering
    \begin{subfigure}{.48\linewidth}
      \includefigure[width=\linewidth]{weighted_min_si}
    \end{subfigure}
    \begin{subfigure}{.48\linewidth}
      \includefigure[width=\linewidth]{weighted_min_em}
    \end{subfigure}
    \caption{Two $\Z$-weighted automata recognizing $\L(\word{a^{2n + b}}) = 1 +
      b$}
  \end{figure}
  \begin{itemize}
  \item $(
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
    \begin{tikzcd}
      \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
      in=125, out=55] \arrow[r, "\triangleleft"] & \Out
    \end{tikzcd}
    )$ in $\Mod{\Z}$
  \item for fields (e.g. $\Q$), everything works
  \item for principal rings (e.g. $\Z$), improper learning only
  \item in general: whole lattice of factorization systems and
    \emph{non-isomorphic} notions of minimality
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \begin{itemize}
  \item $\Z^{A^*}$ does \emph{not} have a basis
  \item when $\L$ is rational
    \begin{itemize}
    \item if $\L$ is $\Z$-valued, $(\Min \L)(\St)$ has a basis
    \item if $\L$ is $\Z[\sqrt{-5}]$-valued, $(\Min \L)(\St)$ is
      \emph{projective}
    \end{itemize}
  \item algorithms: minimization, learning, improper learning
  \end{itemize}
  \begin{figure}
    \centering
    \begin{subfigure}{.48\linewidth}
      \includefigure[width=\linewidth]{weighted_dedekind_min}
    \end{subfigure}
    \begin{subfigure}{.48\linewidth}
      \includefigure[width=\linewidth]{weighted_dedekind}
    \end{subfigure}
    \caption{Two equivalent $\Z[\sqrt{-5}]$-modular automata}
  \end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{The VJGL lemma through automata learning}
  \begin{itemize}
  \item define $a_1 \cdots a_n \le_* w_1a_1w_2 \cdots a_n w_n$ over $A^*$
  \item E := $a?$ | $\{ a_1, \ldots, a_k \}^*$ | E . E
  \item given upwards-closed $U \subseteq A^*$, $\Eval{\sobr{U}}(E)$ decides
    whether $\sem{E} \cap U = \varnothing$
  \item algorithm for learning $U = \up{w_1} \cup \cdots \cup \up{w_n}$ using
    $\Eval{\sobr{U}}$ [Goubeault-Larrecq, 09]
    \begin{itemize}
    \item enumerate words $a_1 \cdots a_n \in A^*$
    \item decide $w \in U$ with $\Eval{\sobr{U}}(a_1? \cdots a_n?)$
    \item maintain $U \supseteq \up{B}$
    \item check whether $U = \up{B}$
      \begin{itemize}
      \item find $(\up{B})^c = \sem{E_1} \cup \cdots \cup \sem{E_m}$
      \item check whether $\sem{E_i} \cap U = \varnothing$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
  \begin{center}
    \includefigure{qo_automaton}
  \end{center}
  \begin{itemize}
  \item upwards-closed set $U \subseteq A^*$ $\Leftrightarrow$ $\L : 1
    \xrightarrow[]{\L(\word{w})} \{ \bot < \top \}$ monotone for ${\le_*}$
  \item $(
    % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BJQgX1PSZc+QigCM5KrUYs2nAMo4Q-QdjwEiAJknV6zVog7sA8kyW8pMKAHN4RUADMAThAC2SMiBwQkE6frl2HCcsOjBrBhgQ6wALcwEQZzdfam8UkAYICDQiMQAOMgdGOBgpBjoAIxgGAAUhdVEQaLiQXRkDEDplBKT3RD80xG1-WUNOYNDwyMiHcwpeIA
    \begin{tikzcd}
      \In \arrow[r, "\triangleright"] & \St \arrow["a"', loop, distance=2em,
      in=125, out=55] \arrow[r, "\triangleleft"] & \Out
    \end{tikzcd}
    )$ in $\Ord$, but
    $(% https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRAB12BlHEAXyogYUAObwioAGYAnCAFskZEDgiLqDCBDREAjAA4ykxnBiCGdAEYwGABUy58hRCGlYRAC17V6zVs4Du-BR8QA
    \begin{tikzcd}
      \St \arrow["w"', loop, distance=2em, in=125, out=55]
    \end{tikzcd})$ is ordered
  \item automata learning implements the VGJL lemma (and provides an
    enumeration)
  \item contributions: exponential upper-bound, learning upwards-closed subsets
    in monoids with subword orderings
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion}
\makesection

%%%%%%%%%%%%%%%%%%%% APPENDIX %%%%%%%%%%%%%%%%%%%%
\appendix

\section{Appendices}

\begin{frame}[noframenumbering]
  \sectionpage
\end{frame}

\end{document}
